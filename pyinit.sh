#!/usr/bin/env bash
#
# Required Notice: Copyright Luis Daniel Reyna Pérez (https://devdevdany.com)
#

# usage: inside the project directory run `pyinit <python_version>`

# uncomment to debug
# set -x

set -Eeuo pipefail
IFS=$'\n\t'
trap_err() {
  exit_status_before_echo=${?}
  echo >&2 "Error at $(caller): ${BASH_COMMAND}"
  exit ${exit_status_before_echo}
}
trap trap_err ERR

python_version="${1}"
environment_name="$(basename "$(pwd)")"

pyenv local "${python_version}"
python -m venv --prompt="${environment_name}" --clear --upgrade-deps ".venv"

.venv/bin/python -m pip install pytest mypy ruff
cat <<_end_of_text >requirements_dev.txt
-r requirements.txt

_end_of_text
.venv/bin/python -m pip freeze >>requirements_dev.txt

touch requirements.txt
touch requirements_lock.txt

curl --location --verbose --output "pyproject.toml" "https://codeberg.org/devdevdany/python-lint-and-format/raw/branch/main/pyproject.toml"

mkdir src
cat <<_end_of_text >src/main.py
from sys import exit


def main():
    return 0


if __name__ == "__main__":
    exit(main())

_end_of_text

echo "The Python environment \"${environment_name}\" was successfully initialized. Remember to run \`source .venv/bin/activate\`"
